/*
 * person.c
 *
 *  Created on: Feb 24, 2020
 *      Author: zamek
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "../include/person.h"

#define MALLOC(ptr,size) 	\
	do {					\
		ptr=malloc(size); 	\
		if (!ptr) 			\
			abort(); 		\
	} while(0)

#define FREE(ptr) 			\
	do {					\
		free(ptr);			\
		ptr=NULL;			\
	} while(0)

static const int BUFFER_SIZE=1024;

#define READ_MODE "r"

static struct PersonRepository {
	int initalized;
	TAILQ_HEAD(p_list, Person) head;
} person_repository = { .initialized=0, .head=NULL };


int person_init(){
	TAILQ_INIT(&person_repository.head);
	person_repository.initalized=1;
	return EXIT_SUCCESS;
}

int person_deinit(){
	struct Person *p;
	while((p=person_remove_head())) {
		FREE(p->name);
		FREE(p->died);
		FREE(p);
	}
	return EXIT_SUCCESS;
}

// apple\0

char *str_chomp(char *line) {
	if (!(line && *line))
		return line;

	char *begin=line;
	while (*line++)
		;

	while (--line>=begin) {
		if (*line >= ' ')
			return begin;

		if (*line == '\r' || *line == '\n') // \r = 0x0a \n=0xd
			*line = '\0';
	}

	return begin;
}


struct Person *person_create(char *name, int age, int statement, char *died) {
	if (!(name && *name))
		return NULL;

	struct Person *result;
	MALLOC(result, sizeof(struct Person));
	result->name = name;
	result->age=age;
	result->state = statement;
	result->died=died;
	return result;
}

/**
 *
 * \return new allocated memory, caller must free it!
 */
struct Person *person_process_file_line(char *line) {
	if (!(line && *line))
		return NULL;

	struct Person *result=NULL;
	char *n = strtok(line, ";");
	char *a = strtok(NULL, ";");
	char *s = strtok(NULL, ";");
	char *d = strtok(NULL, ";");

#ifdef STRICT
	if (n && *n && a && *a && s && *s && d && *d)
		result = person_create(strdup(n),
	   						   atoi(a),
							   atoi(s),
							   strdup(d));
#else
	if (n && *n)
		result = person_create(strdup(n),
							   a ? atoi(a) : 0,
                               s ? atoi(s) : 0,
							   d ? strdup(d) : "?");
#endif
	return result;
}

int person_load_from_file(const char *name){
	if (!(name && *name))
		return EXIT_FAILURE;

	FILE *f=fopen(name, READ_MODE);
	if (!f)
		return EXIT_FAILURE;

	char *line;
	MALLOC(line, BUFFER_SIZE);

	struct Person person=NULL;

	while((line=fgets(line, BUFFER_SIZE, f))) {
		person = person_process_file_line(str_chomp(line));
		person_add(person);
	}

	FREE(line);
	fclose(f);
	return EXIT_SUCCESS;
}

int person_add(struct Person *person){
	if(!(person_repository.initalized && person))
		return EXIT_FAILURE;

	TAILQ_INSERT_TAIL(person_repository.head, person, next);
	return EXIT_SUCCESS;
}

struct Person *person_remove_head(){
}

char *person_get_state(enum PersonState state){
}

void person_print_a_person(struct Person *person){
}

void person_print_all(struct Person *from){
}

struct Person *person_find_by_name(const char *name, struct Person *from){
}

struct Person *person_find_by_age(int age, struct Person *from){
}

struct Person *person_find_by_state(enum PersonState state, struct Person *from){
}

struct Person *person_find_by_died(const char *name, struct Person *from){
}

struct Person *person_first(){
}

struct Person *person_last(){
}

void person_bubble_sort(enum Ordering ordering,
			int (*comparer)(const struct Person *a, const struct Person *b)){
}

int person_compare_by_name(const struct Person *a, const struct Person *b){
}

int person_compare_by_age(const struct Person *a, const struct Person *b){
}

int person_compare_by_state(const struct Person *a, const struct Person *b){
}

int person_compare_by_died(const struct Person *a, const struct Person *b){
}




